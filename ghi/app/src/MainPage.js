import { NavLink } from "react-router-dom"
import Traffic from './assets/Traffic.mp4'
import './index.css';

function MainPage() {
  return (
    <div>      <div>

      <div className='main'>
        <video style={{ position: 'absolute', 'z-index': -1 }} src={Traffic} autoPlay loop muted />
      </div>
        <div className="px-5 py-2 my-5 text-light position-absolute" id="main-view">
          <h1 className="display-1 fw-bold text-center">CarCar</h1>
          <div className="p-2">
            <p className="lead mb-4 display-4 text-center">
                <strong>
                  The premiere solution for
                  automobile dealership management!
              </strong>
            </p>
            <div className="text-center">
                <NavLink to="/inventory/manufacturers/new" className="btn btn-primary shadow">Get started</NavLink>
                <div className="text-center p-2">
                  <NavLink to="/inventory" className="btn btn-warning shadow m-2">View Inventory</NavLink>
                  <NavLink to="/appointments/new" className="btn btn-warning shadow m-2">Make Appointment</NavLink>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
