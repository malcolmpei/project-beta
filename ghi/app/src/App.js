import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentList from './components/appointments/AppointmentList';
import AppointmentForm from './components/appointments/AppointmentForm';
import ManufacturerForm from './components/inventory/ManufacturerForm';
import ManufacturerList from './components/inventory/ManufacturerList';
import VehicleList from './components/inventory/VehicleList';
import VehicleForm from './components/inventory/VehicleForm';
import AutoForm from './components/inventory/AutoForm';
import AutoList from './components/inventory/AutoList';
import SalesPersonForm from './components/sales/SalesPersonForm';
import InventoryList from './components/inventory/InventoryList';
import SalesPersonEdit from './components/sales/SalesPersonEdit';
import CustomerList from './components/sales/CustomerList';
import CustomerForm from './components/sales/CustomerForm';
import CustomerEdit from './components/sales/CustomerEdit';
import SalesList from './components/sales/SalesList';
import SalesForm from './components/sales/SalesForm';
import SalesPersonHistory from './components/sales/SalesPersonHistory';
import Login from './components/accounts/Login';
import AccountDetail from './components/accounts/AccountDetail';
import { useState, useEffect } from 'react';
import Footer from './Footer';
import SalesPersonList from './components/sales/SalesPersonList';
import SalesEdit from './components/sales/SalesEdit';
import ServiceForm from './components/accounts/ServiceForm';



function App() {
  const [token, setToken] = useState()
  const getStatus = async () => {
    const url = 'http://localhost:8080/api/appointments/status'
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json()
      if (data.length === 0) {
        const fetchConfig = {
          method: "post",
          body: JSON.stringify({"state":"Submitted"}),
          headers:{"Content-Type":"application/json"}
        }
        const statusResponse = await fetch(url, fetchConfig)
        if (statusResponse.ok) {
          console.log('added Submitted to appointment database')
        }
      }
    }
  }
  useEffect(() => {
    getStatus()
  },[])
  if (!token) {
    return (
      <BrowserRouter>
        <Nav/>
        <div>
          <Login setToken={setToken}/>
        </div>
      </BrowserRouter>
    )
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className='vh-100'>
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/accounts">
            <Route path="" element={<AccountDetail token={token}/>}/>
            <Route path="service" element={<ServiceForm user={token}/>}/>
          </Route>
          <Route path="/appointments">
            <Route path="" element={<AppointmentList/>}/>
            <Route path="new" element={<AppointmentForm/>}/>
          </Route>
          <Route path="/inventory">
            <Route path="" element={<InventoryList/>}/>
            <Route path="manufacturers">
              <Route path="" element={<ManufacturerList/>}/>
              <Route path="new" element={<ManufacturerForm/>}/>
            </Route>
            <Route path="models">
              <Route path="" element={<VehicleList/>}/>
              <Route path="new" element={<VehicleForm/>}/>
            </Route>
            <Route path="autos">
              <Route path="" element={<AutoList/>}/>
              <Route path="new" element={<AutoForm/>}/>
            </Route>
          </Route>
          <Route path="/sales">

              <Route path="salesperson">
                <Route path="" element={<SalesPersonList/>}/>
                <Route path="new" element={<SalesPersonForm/>}/>
                <Route path="edit/:id" element={<SalesPersonEdit/>}/>
                <Route path="history" element={<SalesPersonHistory/>}/>
              </Route>
            <Route path="" element={<SalesList/>}/>
            <Route path="new" element={<SalesForm/>}/>
            <Route path="edit/:id" element={<SalesEdit/>}/>
            <Route path="customers">
              <Route path="" element={<CustomerList/>}/>
              <Route path="new" element={<CustomerForm/>}/>
              <Route path="edit/:id" element={<CustomerEdit/>}/>
            </Route>
          </Route>
      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
