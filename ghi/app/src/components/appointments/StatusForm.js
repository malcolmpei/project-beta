import { useState } from "react"

function StatusForm() {
    // Setting up hooks
    const [formData, setFormData] = useState({})
    // Saving elements into variables
    const statusSuccess = document.querySelector('#status-success')
    const statusForm = document.querySelector('#status-form')
    // How to save form data
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
    }
    // How to handle submit button
    const handleSubmit = event => {
        event.preventDefault()
        postData()
    }
    // How to handle back button
    const handleBack = () => {
        statusForm.classList.remove('d-none')
        statusSuccess.classList.add('d-none')
    }
    // How to post form data
    const postData = async () => {
        const url = 'http://localhost:8080/api/appointments/status'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newResponse = await response.json()
            statusForm.classList.add('d-none')
            statusSuccess.classList.remove('d-none')
        }
    }
    return (
        <div>
            <div className="container p-2">
                <div className="d-none" id="status-success">
                    <div className="alert alert-success">Successfully added a new status</div>
                    <div className="p-2 text-end">
                        <button onClick={handleBack} className="btn btn-warning">Back</button>
                    </div>
                </div>
                <div className="card p-2" id="status-form">
                    <form onSubmit={handleSubmit}>
                        <h3><strong>ADD A NEW STATUS</strong></h3>
                        <div className="form-floating">
                            <input onChange={handleFormData} className="form-control" name="state"/>
                            <label>Status</label>
                        </div>
                        <div className="text-end">
                            <button className="btn btn-primary m-2">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default StatusForm
