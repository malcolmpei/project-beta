import { useState, useEffect } from "react"
import { BsTrashFill, BsArrowClockwise } from 'react-icons/bs'

function StatusList() {
    // Setting up hooks
    const [status, setStatus] = useState([])
    const [refresh, setRefresh] = useState(0)
    // How to get list of all status
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/status')
        if (response.ok) {
            const data = await response.json()
            setStatus(data.status)
        }
    }
    // Change list of all status everytime you hit refresh button
    useEffect(() => {
        getData()
    },[refresh])
    // How to handle delete button
    const handleDeleteStatus = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/status/${id}`, {
                method: 'delete'
            })
            if (response.ok) {
                setStatus(status.filter(state => state.id !== id))
            }
        }
        catch (error) {
            console.error(error)
        }
    }
    // How to handle refresh button
    const handleRefresh = () => {
        setRefresh(refresh + 1)
    }
    return (
        <div>
            <div className="container text-center">
                <div className="card p-2 m-2">
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>STATUS LIST</th>
                                <th className="text-end">
                                    <button onClick={handleRefresh} className="btn btn-warning" style={{fontSize:"20px"}}><BsArrowClockwise/></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {status.map(state => {
                                return (
                                    <tr key={state.state}>
                                        <td>{state.state}</td>
                                        <td className="text-end">
                                            <button onClick={() => handleDeleteStatus(state.id)} className="btn btn-danger">
                                                <BsTrashFill style={{fontSize:"20px"}}/>
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}
export default StatusList
