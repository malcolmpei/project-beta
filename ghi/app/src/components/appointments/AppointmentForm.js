import { useEffect, useState } from "react"
import { NavLink } from "react-router-dom"

function AppointmentForm(props) {
    console.log(props.user)
    const backgroundPhoto = "https://i.pinimg.com/564x/2a/fe/c5/2afec57a5e5d7508e265964df8fa36ab.jpg"
    // Setting up Hooks
    const [formData, setFormData] = useState({status: "Submitted"})
    const [technicians, setTech] = useState([])
    // How to handle form data
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value

        })
    }
    // How to post data
    const postData = async () => {
        const url = 'http://localhost:8080/api/appointments/'
        const fetchConfigs = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchConfigs)
        if (response.ok) {
            const newResponse = await response.json()
            const successTag = document.querySelector('#appointment-created')
            const formTag = document.querySelector('#appointment-form')
            successTag.classList.remove('d-none')
            formTag.classList.add('d-none')
        }
        else {
            const noStatus = document.querySelector('#no-status')
            noStatus.classList.remove('d-none')
        }
    }

    // How to handle submit button
    const handleSubmit = event => {
        event.preventDefault()
        postData()
    }
    // How to fetch list of sale employees
    const getSales = async () => {
        const response = await fetch('http://localhost:8090/api/salesperson')
        if (response.ok) {
            const data = await response.json()
            setTech(data.employees)
        }
    }
    // Fetching list of employees once
    useEffect(() => {
        getSales()
    }, [])
    return (
        <div>
            <div className="position-fixed end-0">
                <h1 className="text-light text-end p-5 m-5"><strong>
                    <div>REGISTER A</div>
                    <div>APPOINTMENT</div>
                    <div>INTO THE SYSTEM</div>
                </strong></h1>
            </div>
            <div className="position-fixed end-50">
                <div className="container m-5">
                    <div className="d-none text-center" id="appointment-created">
                        <div className="alert alert-success" >Successfully Created</div>
                        <NavLink to="/appointments" className="btn btn-success">Back</NavLink>
                    </div>
                    <div className="card shadow p-2 row" id="appointment-form">
                        <div className="card-title text-center">
                            <h1><strong>NEW APPOINTMENT</strong></h1>
                        </div>
                        <div className="card p-2">
                            <form onSubmit={handleSubmit}>
                                <div className="row p-2">
                                    <div className="col">
                                        <div className="form-floating">
                                            <input onChange={handleFormData} type="text" name="owner"required className="form-control"/>
                                            <label>Owner Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating">
                                            <select onChange={handleFormData} name='technician' className="form-select" required>
                                                <option value=""></option>
                                                {technicians.map(employee => {
                                                    return (
                                                        <option key={employee.href} value={employee.href}>{employee.employee_name}</option>
                                                    )
                                                })}
                                            </select>
                                            <label>Choose your Technician</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row p-2">
                                    <div className="col">
                                        <div className="form-floating">
                                            <input onChange={handleFormData} name="date" type="datetime-local" id="appointment-date" className="form-control" required/>
                                            <label>Appointment Date</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating">
                                            <input onChange={handleFormData} name="vin" className="form-control" required maxLength="16"/>
                                            <label>VIN #</label>
                                        </div>
                                    </div>
                                </div>
                                <div className="row p-2">
                                    <div className="form-floating">
                                        <input onChange={handleFormData} name="reason" type="text" className="form-control" required />
                                        <label>Reasons</label>
                                    </div>
                                </div>
                                <div className="alert alert-danger d-none" id="no-status">Something went wrong, try again</div>
                                <div className="text-center p-2">
                                    <button className="btn btn-primary p-2">Save</button>
                                </div>
                            </form>
                            <div className="text-center">
                                <NavLink className="btn btn-warning" to="/appointments">Back</NavLink>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <img src={backgroundPhoto} className="w-100"/>
        </div>
    )
}

export default AppointmentForm
