function AppointmentDelete(props) {
    // How to handle delete button
    const handleDelete = () => {
        deleteData()
    }
    // How to handle cancel button
    const handleCancel = () => {
        const confirmTag = document.querySelector('#delete-appointment')
        const deleteTag = document.querySelector('#delete-button')
        confirmTag.classList.add('d-none')
        deleteTag.classList.remove('d-none')
    }
    // Fetching request to delete appointment
    const deleteData = async () => {
        const url = `http://localhost:8080/api/appointments/${props.id}`
        const fetchConfigs = {
            method: "Delete"
        }
        const response = await fetch(url, fetchConfigs)
        if (response.ok) {
            const newResponse = await response.json()
            window.location.reload(false)
        }
    }
    return (
        <div className="d-none" id="delete-appointment">
            <div>
                <button onClick={handleDelete} className="btn btn-danger w-100">Confirm</button>
                <button onClick={handleCancel} className="btn btn-warning w-100">Cancel</button>
            </div>
        </div>
    )
}
export default AppointmentDelete
