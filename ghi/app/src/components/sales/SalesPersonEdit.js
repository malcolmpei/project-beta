import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

function SalesPersonEdit() {
    // use the useParams hook to get the id from the URL
    const { id } = useParams();

    // create a state variable to hold the salesperson details
    const [salesperson, setSalesperson] = useState({});

    // use useEffect to fetch the salesperson data from the server when the component is first rendered
    useEffect(() => {
        const fetchData = async () => {
            try {
                // specify the url to get the salesperson data
                const salespersonUrl = `http://localhost:8090/api/salesperson/${id}`;
                // make a GET request to the server
                const response = await fetch(salespersonUrl);
                // parse the response as json
                const data = await response.json();
                // update the state variable with the salesperson data
                setSalesperson(data);
            } catch (error) {
                console.error(error);
            }
        };
        // call the fetchData function
        fetchData();
    }, [id]); // include the id in the dependency array

    // handle the form submission
    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            // specify the url to update the salesperson data
            const updateUrl = `http://localhost:8090/api/salesperson/${id}`;
            // make a PUT request to the server
            await fetch(updateUrl, {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(salesperson)
            });
            // redirect the user to the list page
            window.location.href = '/sales';
        } catch (error) {
            console.error(error);
        }
    };


    return (
        <div className='container p-2'>
            <form onSubmit={handleSubmit}>
                <div className='form-group'>
                    <label htmlFor='employee_name'>Employee Name</label>
                    <input
                        type='text'
                        className='form-control'
                        id='employee_name'
                        defaultValue={salesperson.employee_name}
                        onChange={e => setSalesperson({...salesperson, employee_name: e.target.value})}
                    />
                </div>
                <div className='form-group'>
                    <label htmlFor='employee_number'>Employee Number</label>
                    <input
                        type='text'
                        className='form-control'
                        id='employee_number'
                        defaultValue={salesperson.employee_number}
                        onChange={e => setSalesperson({...salesperson, employee_number: e.target.value})}
                    />
                </div>
                <button type='submit' className='btn btn-primary'>Save Changes</button>
            </form>
        </div>
    );
    }
    export default SalesPersonEdit;
