import React, { useState, useEffect } from 'react';
import { Table, Form, FormGroup, Label, Input } from 'reactstrap';
import {NavLink } from 'react-router-dom';


function SalesPersonHistory() {
  const [salespersons, setSalespersons] = useState([]);
  const [personId, setSelectedSalesperson] = useState();
  const [sales, setSales] = useState({});

  const fetchSales = async () => {
    const response = await fetch('http://localhost:8090/api/sales')
    if (response.ok) {
      const data = await response.json()
      setSales(data.sales)
    }
  }
  const fetchSalespersons = async () => {
    // Use fetch API to get the data from the server
    const response = await fetch('http://localhost:8090/api/salesperson/');
    const data = await response.json();
    setSalespersons(data.employees);
  }
  function filteredList(id) {
    let result = []
    for (let sale of sales) {
      if (sale.sales_person.href === id) {
        result.push(sale)
      }
    }
    return result
  }
  const handleSalespersonChange = event => {
    setSelectedSalesperson(event.target.value);
    filteredList(personId)
  }
  useEffect(() => {
    fetchSalespersons();
    fetchSales()
  }, []);
  return (

    <div className='container p-2'>
      <h1>Sales Person History</h1>
      <Form>
        <FormGroup>
          <Label for="exampleSelect">Select Salesperson</Label>
          <Input type="select" name="select" id="exampleSelect" onChange={handleSalespersonChange}>
            <option value="">Select</option>
            {salespersons.map((salesperson) => (
              <option key={salesperson.id} value={salesperson.href}>
                {salesperson.employee_name}
              </option>
            ))}
          </Input>
        </FormGroup>
      </Form>

      {personId && (
        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Customer Name</th>
              <th>VIN</th>
              <th>Sale Price</th>
            </tr>
          </thead>
          <tbody>
            {filteredList(personId).map(sale => {
              return (
                <tr key={sale.id}>
                  <td>{sale.sales_person.employee_name}</td>
                  <td>{sale.customer.name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>{sale.price}</td>
                </tr>
              )
            })}
          </tbody>

        </Table>
      )}
      <NavLink type="button" className='btn btn-warning' to="/sales">Back</NavLink>
    </div>
  );
}

export default SalesPersonHistory;
