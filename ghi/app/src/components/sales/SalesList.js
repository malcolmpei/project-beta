import { useState, useEffect } from "react"
import { NavLink } from "react-router-dom"


function SalesList() {
    const [sales, setSales] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales')
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }
    useEffect(() => {
        getData()
    },[])

    const handleDelete = async (id) => {
        try {
            const response = await fetch(`http://localhost:8090/api/sales/${id}`, {
                method: 'DELETE'
            });
            if(response.ok) {
                // Remove the deleted sale from the state
                setSales(sales.filter(sale => sale.id !== id));
            }
        } catch(error) {
            console.error(error);
        }
    }
    return (
        <div>
            <div id="sales-nav-bar">
                <div className="container p-2 text-center">
                    <NavLink to="./customers/new" className="btn btn-success m-2">Add Customer</NavLink>
                    <NavLink to="./salesperson/new" className="btn btn-success m-2">Add Employee</NavLink>
                    <NavLink to="./new" className="btn btn-success m-2">New Sale</NavLink>
                    <NavLink to="./salesperson/history" className="btn btn-success m-2">Sales Person History</NavLink>
                    <NavLink to="./salesperson" className="btn btn-success m-2">Employee List</NavLink>
                    <NavLink to="./customers" className="btn btn-success m-2">Customer List</NavLink>
                </div>
            </div>
            <div className="container p-2">
                <div className="" id="sale-list-view">
                    <div className="row">
                        <div className="col">
                            <h1>Sales History</h1>
                        </div>
                    </div>
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Sales Person</th>
                                <th>Automobile VIN#</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sales.map(sale => {
                                return (
                                    <tr key={sale.id}>
                                        <td>{sale.customer.name}</td>
                                        <td>{sale.sales_person.employee_name}</td>
                                        <td>{sale.automobile.vin}</td>
                                        <td>{sale.price}</td>
                                        <td>
                                            <NavLink to={`/sales/edit/${sale.id}`} className="btn btn-primary m-2">Edit</NavLink>
                                            <button className="btn btn-danger m-2" onClick={() => handleDelete(sale.id)}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    )
}

export default SalesList
