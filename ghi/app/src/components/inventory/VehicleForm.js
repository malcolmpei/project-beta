import { useState, useEffect } from "react"
import { NavLink } from "react-router-dom"

function VehicleForm() {
    const [manufacturers, setManufacturers] = useState([])
    const [formData, setFormData] = useState({})
    const backgroundPhoto = "https://www.rivervaleleasing.co.uk/up-images/green-car.jpg"
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
    }
    const handleSubmit = event => {
        event.preventDefault()
        postData()
    }
    const postData = async () => {
        const url = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers:{"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newResponse = await response.json()
            const vehicleSuccess = document.querySelector('#vehicle-success')
            const vehicleForm = document.querySelector('#vehicle-form')
            vehicleForm.classList.add('d-none')
            vehicleSuccess.classList.remove('d-none')
        }
    }
    const getData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {
        getData()
    }, [])
    return (
        <div>
            <div className="position-fixed start-0 container p-5" id="model-form">
                <div className="container p-5">
                    <div className="d-none" id="vehicle-success">
                        <div className="alert alert-success text-center">Successfully added vehicle</div>
                        <div className="text-center">
                            <NavLink to="/inventory/autos/new" className="btn btn-primary">Continue</NavLink>
                        </div>
                    </div>
                    <div className="container" id="vehicle-form">
                        <form onSubmit={handleSubmit}>
                            <div className="row p-2">
                                <div className="form-floating col">
                                    <input onChange={handleFormData} className="form-control" name="name"/>
                                    <label>Name</label>
                                </div>
                                <div className="form-floating col">
                                    <select onChange={handleFormData} className="form-select" name="manufacturer_id" required>
                                        <option value=""></option>
                                        {manufacturers.map(m => {
                                            return (
                                                <option key={m.id} value={m.id}>{m.name}</option>
                                            )
                                        })}
                                    </select>
                                    <label>Manufacturer</label>
                                </div>
                            </div>
                            <div className="form-floating p-2">
                                <input onChange={handleFormData} className="form-control" name="picture_url"/>
                                <label>Picture URL</label>
                            </div>
                            <div className="p-2 text-center">
                                <button className="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div className="position-fixed top-50 start-50">
                <h1 className="text-light text-end p-5 m-5"><strong>
                    <div>REGISTER</div>
                    <div>YOUR VEHICLE</div>
                    <div>INTO THE</div>
                    <div>DATABASE</div>
                </strong></h1>
            </div>
            <div>
                <img src={backgroundPhoto} className="vh-100 vw-100"/>
            </div>
        </div>
    )
}
export default VehicleForm
