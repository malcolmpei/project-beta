import { useState, useEffect } from "react"
import { NavLink } from "react-router-dom"

function AutoForm() {
    const [vehicles, setVehicles] = useState([])
    const [formData, setFormData] = useState({})
    const backgroundPhoto = "https://thumbs.dreamstime.com/b/vintage-green-car-60-s-3646288.jpg"
    const handleFormData = event => {
        setFormData({
            ...formData,
            [event.target.name]:event.target.value
        })
    }

    const handleSubmit = event => {
        event.preventDefault()
        postData()
    }

    const postData = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {"Content-Type":"application/json"}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newResponse = await response.json()
            const successMessage = document.querySelector('#auto-success')
            const autoForm = document.querySelector('#auto-form')
            successMessage.classList.remove('d-none')
            autoForm.classList.add('d-none')
        }
    }
    const getData = async () => {
        const url = 'http://localhost:8100/api/models'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setVehicles(data.models)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <div className="position-fixed end-0 p-5 m-5">
                <div className="d-none" id="auto-success">
                    <div className="alert alert-success">Successfully saved Automobile</div>
                    <div className="text-center">
                        <NavLink to="/" className="btn btn-primary">Continue</NavLink>
                    </div>
                </div>
                <div className="container p-2" id="auto-form">
                    <div className="form-floating">
                        <form onSubmit={handleSubmit}>
                            <div className="container p-2">
                                <div className="form-floating p-2">
                                    <select onChange={handleFormData} name="model_id" className="form-select">
                                        <option value=""></option>
                                        {vehicles.map(v => {
                                            return (
                                                <option key={v.id} value={v.id}>{v.name}</option>
                                            )
                                        })}
                                    </select>
                                    <label>Vehicle model</label>
                                </div>
                                <div className="row p-2">
                                    <div className="form-floating col">
                                        <input onChange={handleFormData} className="form-control" name="color"/>
                                        <label>Color</label>
                                    </div>
                                    <div className="form-floating col">
                                        <input onChange={handleFormData} type="number" className="form-control" name="year" maxLength="4"/>
                                        <label>Year</label>
                                    </div>
                                </div>
                                <div className="form-floating p-2">
                                    <input onChange={handleFormData} className="form-control" name="vin" maxLength="16"/>
                                    <label>VIN number</label>
                                </div>
                                <div className="text-end">
                                    <button className="btn btn-primary m-2">Register</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div>
                <h1 className="position-absolute start-0 p-5 m-5 text-light"><strong>
                    <div>FINALLY</div>
                    <div>REGISTER</div>
                    <div>YOUR</div>
                    <div>AUTOMOBILE</div>
                </strong></h1>
            </div>
            <div>
                <img src={backgroundPhoto} className="vh-100 vw-100"/>
            </div>
        </div>
    )
}

export default AutoForm
