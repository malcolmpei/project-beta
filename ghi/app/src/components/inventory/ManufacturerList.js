import { useState, useEffect } from "react"

function ManufacturerList() {
    // Setting up hooks
    const [manufacturers, setManufacturers] = useState([])
    const [filtered, setQuery] = useState()
    // Default info to populate page
    const defaultPhoto = "https://content.tgstatic.co.nz/webassets/assets/images/default-car.png"
    const defaultText = "The best, coolest, most innovative automotive manufacturer in the world! If it only that was true."
    // How to get list of all manufacturers
    const getData = async () => {
        const url = 'http://localhost:8100/api/manufacturers'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }
    // Getting list of manufacturers once
    useEffect(() => {
        getData()
    }, [])
    // Handle search query
    const handleSearch = event => {
        SearchByName(event.target.value, manufacturers)
    }
    // Handle search filter
    function SearchByName(q, list) {
        if (!q) {
            setQuery(list)
        }
        else {
            let result = []
            manufacturers.map(manu => {
                if (manu.name.toUpperCase().includes(q.toUpperCase())) {
                    result.push(manu)
                }
            })
            let unique = [...new Set(result)]
            setQuery(unique)
        }
    }
    // If you use filtered list instead
    function ManufacturerList() {
        if (filtered) {
            return filtered
        }
        return manufacturers
    }
    return (
        <div className="container">
            <div className="row">
                <h1 className="p-2 col"><strong>MANUFACTURERS</strong></h1>
                <div className="col form-floating p-2">
                    <input onChange={handleSearch} type="search" className="form-control"/>
                    <label>Search...</label>
                </div>
            </div>
            <div className="p-2">
                <div className="row justify-content-center">
                {ManufacturerList().map(manu => {
                    return (
                        <div key={manu.id} className="card col-sm-4 m-2" style={{width:"400px"}}>
                            <div className="rounded p-2">
                                <img src={defaultPhoto} className="card-img-top"/>
                                <h3 className="text-center"><strong>{manu.name}</strong></h3>
                                <div className="text-center text-muted">1941-present</div>
                                <div className="card-body">{defaultText}</div>
                            </div>
                        </div>
                    )
                })}
                </div>
            </div>
        </div>
    )
}

export default ManufacturerList
