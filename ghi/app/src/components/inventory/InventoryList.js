import AutoList from "./AutoList"
import ManufacturerList from "./ManufacturerList"
import VehicleList from "./VehicleList"
import { useState } from "react"
import { NavLink } from "react-router-dom"

function InventoryList() {
    const [option, setOption] = useState("")
    const handleOptions = event => {
        setOption(event.target.value)
    }
    function FilterView(option) {
        if (option.option === "1") {
            return (
                <ManufacturerList/>
            )
        }
        else if (option.option === "2") {
            return (
                <VehicleList/>
            )
        }
        else if (option.option === "3") {
            return (
                <AutoList/>
            )
        }
        else {
            return (
                <div className="text-center container">
                    <div className="alert alert-info">No filter choosen</div>
                </div>
            )
        }
    }
    return (
        <div>
            <div className="container">
                <div id="create-buttons">
                    <div className="row">
                        <NavLink to="./manufacturers/new" className="col btn btn-success m-2">Add Manufacturers</NavLink>
                        <NavLink to="./models/new" className="col btn btn-success m-2">Add Vehicles</NavLink>
                        <NavLink to="./autos/new" className="col btn btn-success m-2">Add Automobiles</NavLink>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="p-2">
                    <div id="category-select-form" className="">
                        <h3>FILTER INVENTORY BY</h3>
                        <div className="form-floating">
                            <select onChange={handleOptions} className="form-select">
                                <option value=""></option>
                                <option value="1">Manufacturers</option>
                                <option value="2">Vehicles</option>
                                <option value="3">Automobiles</option>
                            </select>
                            <label>Category</label>
                        </div>
                    </div>
                </div>
            </div>
            <div id="filter-view" className="p-5">
                <FilterView option={option}/>
            </div>
        </div>
    )
}

export default InventoryList
