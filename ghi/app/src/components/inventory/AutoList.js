import { useState, useEffect } from "react"
import { BsFillCartFill, BsFillGeoAltFill, BsFillLightningFill, BsTagFill } from "react-icons/bs"

function AutoList() {
    const [autos, setAutos] = useState([])
    const [filtered, setQuery] = useState()
    const [sales, setSales] = useState([])
    const getSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales')
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }
    const getData = async () => {
        const url = 'http://localhost:8100/api/automobiles'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setAutos(data.autos)
        }
    }
    useEffect(() => {
        getData()
        getSales()
    }, [])
    // Handle search query
    const handleSearch = event => {
        SearchByName(event.target.value, autos)
    }
    // Handle search filter
    function SearchByName(q, list) {
        if (!q) {
            setQuery(list)
        }
        else {
            let result = []
            autos.map(auto => {
                if (auto.vin.toUpperCase().includes(q.toUpperCase())) {
                    result.push(auto)
                }
                if (auto.model.name.toUpperCase().includes(q.toUpperCase())) {
                    result.push(auto)
                }
                if (auto.model.manufacturer.name.toUpperCase().includes(q.toUpperCase())) {
                    result.push(auto)
                }
            })
            let unique = [...new Set(result)]
            setQuery(unique)
        }
    }
    // If you use filtered list instead
    function FilteredList() {
        if (filtered) {
            return filtered
        }
        return autos
    }
    function IsSold(props) {
        const vin = props.vin
        for (let sale of sales) {
            if (vin === sale.automobile.vin) {
                return (
                    <BsTagFill className="text-danger"/>
                )
            }
        }
        return (<BsFillCartFill className="text-success"/>)
    }
    return (
        <div className="container p-2">
            <div className="row">
                <h1 className="col"><strong>AUTOMOBILES</strong></h1>
                <div className="col"></div>
                <div className="col text-end form-floating">
                    <input onChange={handleSearch} type="search" className="form-control"/>
                    <label>Search...</label>
                </div>
            </div>
            <br></br>
            <div className="row justify-content-center">
                {FilteredList().map(auto => {
                    return (
                        <div key={auto.id} className="col-4 rounded p-2" style={{width:"440px", overflow:"hidden"}}>
                            <div className="card p-2">
                                <div className="row">
                                    <div className="col">
                                        <h3 className=""><strong>{auto.model.name}</strong></h3>
                                        <div className="text-muted">
                                            <div>{auto.year} {auto.model.manufacturer.name}</div>
                                            <div>Available for delivery</div>
                                        </div>
                                    </div>
                                    <div className="col text-end">
                                        <h4>$45.670</h4>
                                        <div className="text-muted">$568/month</div>
                                        <IsSold vin={auto.vin}/>
                                    </div>
                                    <div>
                                        <div style={{width:"400px", height:"200px", overflow:"hidden"}}>
                                            <img src={auto.model.picture_url} className="card-img"/>
                                        </div>
                                        <div className="row">
                                            <div className="col">
                                                <span><strong>520</strong></span>
                                                <span>mi</span>
                                                <div className="text-muted">Range (Not tested)</div>
                                            </div>
                                            <div className="col text-center">
                                                <span><strong>120</strong></span>
                                                <span>mph</span>
                                                <div className="text-muted">Top speed</div>
                                            </div>
                                            <div className="col text-end">
                                                <span><strong>1.5</strong></span>
                                                <span>s</span>
                                                <div className="text-muted">0-60 mph</div>
                                            </div>
                                        </div>
                                        <br></br>
                                        <div className="row text-muted">
                                            <div className="col">
                                                <div>Worst color imagineable</div>
                                                <div>No-wheel drive</div>
                                                <div>All wood no metal</div>
                                            </div>
                                            <div className="col">
                                                <div><BsFillGeoAltFill/> It can go from A to B</div>
                                                <div><BsFillLightningFill/> Chance of electrocution</div>
                                            </div>
                                        </div>
                                        <div className="form-floating text-center">
                                            <div className="form-control"><strong>{auto.vin}</strong></div>
                                            <label>VIN#</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        )
                    })}
            </div>
        </div>
    )
}

export default AutoList
