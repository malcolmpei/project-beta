import { useState, useEffect } from "react"
import { NavLink } from "react-router-dom"
function AccountDetail(props) {
    const token = props.token
    const [account, setAccount] = useState({})
    const [accounts, setAccounts] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8200/api/accounts')
        if (response.ok) {
            const data = await response.json()
            for (let account of data.accounts) {
                if (account.first_name === token.first_name && account.last_name === token.last_name) {
                    setAccount(account)
                }
            }
            setAccounts(data.accounts)
        }
    }
    useEffect(() => {
        getData()
    },[])

    return (
        <div>
            <div className="text-center p-2">
                <NavLink to="./service" className="btn btn-success">Make an appointment</NavLink>
            </div>
            <div className="container p-2 row">
                <div className="container p-5 col text-center">
                    <h1>Hi {account.first_name} {account.last_name}!</h1>
                    <div className="container">
                        <div>{account.email}</div>
                    </div>
                </div>
                <div className="container p-2 shadow col">
                    <div>
                        <h3>List of accounts</h3>
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th>First name</th>
                                    <th>Last name</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                {accounts.map(account => {
                                    return(
                                        <tr key={account.id}>
                                            <td>{account.first_name}</td>
                                            <td>{account.last_name}</td>
                                            <td>{account.email}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AccountDetail
