from django.urls import path

from .views import (api_sales_person,
 api_list_sales_person,
 api_list_automobilevo,
 api_customer,
 api_list_customers,
 api_list_sales,
 api_sale)



urlpatterns = [
    path("salesperson/", api_list_sales_person, name="list_sale_person"),
    path("salesperson/<int:pk>", api_sales_person, name="show_sale_person"),
    path("autos/", api_list_automobilevo, name='list_autos'),
    path("customers/", api_list_customers, name='list_customers'),
    path("customers/<int:pk>", api_customer, name="show_customer"),
    path("sales/", api_list_sales, name='list_sales'),
    path("sales/<int:pk>", api_sale, name="show_sale"),
    ]
