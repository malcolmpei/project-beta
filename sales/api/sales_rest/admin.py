from django.contrib import admin

from .models import AutomobileVO, SalesPerson, PotentialCustomer, Sales

admin.site.register(AutomobileVO)
admin.site.register(SalesPerson)
admin.site.register(PotentialCustomer)
admin.site.register(Sales)
