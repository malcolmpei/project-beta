

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, SalesPerson, PotentialCustomer, Sales
import json
from .encoders import SalesEncoder, PotentialCustomerEncoder, SalesPersonEncoder, AutomobileVOEncoder



@require_http_methods(["GET", "DELETE", "PUT"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except Sales.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=pk)

            props = ["employee_name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_sales_person(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"employees":sales_persons},
            encoder=SalesPersonEncoder)
    else:
        content = json.loads(request.body)

    sales_persons = SalesPerson.objects.create(**content)
    return JsonResponse(sales_persons, encoder=SalesPersonEncoder, safe=False)

@require_http_methods(["GET"])
def api_list_automobilevo(request):
    autos = AutomobileVO.objects.all()
    return JsonResponse(
        {"autos": autos},
        encoder = AutomobileVOEncoder,
    )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = PotentialCustomer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=PotentialCustomerEncoder,
                safe=False
            )
        except PotentialCustomer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = PotentialCustomer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except PotentialCustomer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customer = PotentialCustomer.objects.get(id=pk)

            props = ["name", "address", "phone"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=PotentialCustomerEncoder,
                safe=False,
            )
        except PotentialCustomer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder=PotentialCustomerEncoder)
    else:
        content = json.loads(request.body)
        customers = PotentialCustomer.objects.create(**content)
        return JsonResponse(customers, encoder=PotentialCustomerEncoder, safe=False)

@require_http_methods(["GET", "DELETE", "PUT"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sales.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False
            )
        except Sales.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = Sales.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except Sales.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customer = PotentialCustomer.objects.get(id=content["customer"])
            content["customer"] = customer
            sales_person = SalesPerson.objects.get(employee_name=content["sales_person"])
            content["sales_person"] = sales_person
            automobile = AutomobileVO.objects.get(import_href=content["automobile"])
            content["automobile"] = automobile
            Sales.objects.filter(id=pk).update(**content)
            sale = Sales.objects.get(id=pk)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except Sales.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sales.objects.all()
        return JsonResponse(
            {"sales":sales},
            encoder=SalesEncoder)
    else:
        content = json.loads(request.body)
        try:
            customer = PotentialCustomer.objects.get(id=content['customer'])
            content['customer'] = customer
        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"error": "customer id does not exist"}
            )
        try:
            saleperson = SalesPerson.objects.get(employee_name=content['sales_person'])
            content['sales_person'] = saleperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"error": "sale person href does not exist"}
            )
        try:
            automobile = AutomobileVO.objects.get(import_href=content['automobile'])
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"error": "automobile import href does not exist"}
            )
        sales = Sales.objects.create(**content)
        return JsonResponse(sales, encoder=SalesEncoder, safe=False)
