from common.json import ModelEncoder
from .models import TechnicianVO, Status, Appointment

class TechnicianVOEncoder(ModelEncoder):
    model = TechnicianVO
    properties = [
        "import_href",
        "employee_name"
    ]

class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["state", "id"]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'owner',
        'date',
        'reason',
        'vin',
        "id"
    ]
    def get_extra_data(self, o):
        return {
            "technician":o.technician.employee_name,
            "status":o.status.state
        }

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "owner",
        "date",
        "reason",
        "vin",
        "technician",
        "status",
        "id"
    ]
    encoders={
        "technician":TechnicianVOEncoder(),
        "status":StatusEncoder()
    }
