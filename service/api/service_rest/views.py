from django.shortcuts import render
from .models import Appointment, TechnicianVO, Status
from .encoders import AppointmentEncoder, AppointmentDetailEncoder, StatusEncoder
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments":appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            technician = TechnicianVO.objects.get(import_href=content['technician'])
            content['technician'] = technician
            status = Status.objects.get(state=content['status'])
            content['status'] = status
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except TechnicianVO.DoesNotExist:
            response = JsonResponse({"Technician": "Does not exist, make sure you are using href"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.filter(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            technician = TechnicianVO.objects.get(import_href=content['technician'])
            content['technician'] = technician
        except TechnicianVO.DoesNotExist:
            response = JsonResponse({"technician": "Does not exist"})
            response.status_code = 404
            return response
        try:
            status = Status.objects.get(state=content['status'])
            content['status'] = status
        except Status.DoesNotExist:
            response = JsonResponse({"status": "Does not exist"})
            response.status_code = 404
            return response
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.filter(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_status(request):
    if request.method == "GET":
        status = Status.objects.all()
        return JsonResponse(
            {"status":status},
            encoder=StatusEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        status = Status.objects.create(**content)
        return JsonResponse(
            content,
            encoder=StatusEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_status(request, pk):
    if request.method == "GET":
        try:
            status = Status.objects.get(id=pk)
            return JsonResponse(
                status,
                encoder=StatusEncoder,
                safe=False
            )
        except Status.DoesNotExist:
            response = JsonResponse({"error":"Status does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            count, _ = Status.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Status.DoesNotExist:
            return JsonResponse({"message":"Status not found"})
    else:
        content = json.loads(request.body)
        Status.objects.filter(id=pk).update(**content)
        status = Status.objects.filter(id=pk)
        return JsonResponse(
            status,
            encoder=StatusEncoder,
            safe=False
        )
