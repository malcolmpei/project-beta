import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
from service_rest.models import TechnicianVO
# from service_rest.models import Something

def get_salesPerson():
    response = requests.get('http://sales-api:8000/api/salesperson/')
    content = json.loads(response.content)
    for person in content['employees']:
        TechnicianVO.objects.update_or_create(
            import_href=person['href'],
            defaults={"employee_name":person['employee_name']}
        )

def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_salesPerson()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
