from django.shortcuts import render
from .models import Account
from django.http import JsonResponse
from .encoders import AccountEncoder
import json
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET","POST"])
def api_account(request):
    if request.method == "GET":
        accounts = Account.objects.all()
        return JsonResponse(
            {"accounts":accounts},
            encoder=AccountEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        accounts = Account.objects.create(**content)
        return JsonResponse(
            {"accounts":accounts},
            encoder=AccountEncoder,
            safe=False
        )


@require_http_methods(["GET","PUT","DELETE"])
def api_show_account(request, pk):
    if request.method == "GET":
        account = Account.objects.filter(id=pk)
        return JsonResponse(
            account,
            encoder=AccountEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        try:
            count, _ = Account.objects.get(id=pk).delete()
        except Account.DoesNotExist:
            return JsonResponse(
                {"message":"account not found"}
            )
        return JsonResponse(
            {"deleted": count > 0}
        )
